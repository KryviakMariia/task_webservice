package com.kryviak.service.rest;

import com.kryviak.MessagesConstants;
import com.kryviak.bo.NewsPaperBO;
import com.kryviak.model.NewsPaperModel;
import com.kryviak.utils.CsvController;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/rest")
public class NewsPaperService implements MessagesConstants {

    private NewsPaperBO newsPaperBO = new NewsPaperBO();

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllNewsPapers() {
        return Response
                .status(Response.Status.OK)
                .entity(new CsvController().getAllNews())
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

    @GET
    @Path("/get")
    public Response getNewsByGenre(@QueryParam("genre") String genre) {
        if (new NewsPaperBO().findNewsByGenre(genre).size() != 0) {
            return Response
                    .status(Response.Status.OK)
                    .entity(new NewsPaperBO().findNewsByGenre(genre))
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(NO_GENRE_FOUND)
                    .type(MediaType.TEXT_PLAIN)
                    .build();
        }
    }

    @DELETE
    @Path("/delete")
    public Response deleteNewsByTitle(@QueryParam("title") String title) {
        if (newsPaperBO.deleteNewsByTitle(title)) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity(NEWSPAPER_WAS_DELETE)
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(NO_TITLE_FOUND)
                    .build();
        }
    }

    @POST
    @Path("/post/news")
    @Produces(MediaType.TEXT_PLAIN)
    public Response addNews(NewsPaperModel newsPaperModel) {
        if (newsPaperBO.addNews(newsPaperModel)) {
            return Response
                    .status(Response.Status.CREATED)
                    .entity(NEWSPAPER_WAS_ADDED)
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(NEWSPAPER_WAS_NOT_EXIST)
                    .build();
        }
    }

    @PUT
    @Path("/edit")
    @Produces(MediaType.TEXT_PLAIN)
    public Response changeNews(NewsPaperModel newsPaperModel) {
        if (!newsPaperBO.isObjectValid(newsPaperModel)) {
            return Response
                    .status(Response.Status.OK)
                    .entity(NEWSPAPER_WAS_NOT_EXIST)
                    .build();
        } else if (!newsPaperBO.isTitlePresent(newsPaperModel.getTitle())) {
            return Response
                    .status(Response.Status.OK)
                    .entity(NO_TITLE_FOUND)
                    .build();
        } else {
            newsPaperBO.editNews(newsPaperModel);
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity(NEWSPAPER_WAS_CHANGE)
                    .build();
        }
    }
}