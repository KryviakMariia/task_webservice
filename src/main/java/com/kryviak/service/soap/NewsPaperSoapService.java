package com.kryviak.service.soap;

import com.kryviak.model.NewsPaperModel;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface NewsPaperSoapService {
    @WebMethod
    List<NewsPaperModel> getAllNews();

    @WebMethod
    List<NewsPaperModel> getNews(String genre);

    @WebMethod
    boolean addNews(NewsPaperModel newsPaperModel);

    @WebMethod
    boolean removeNews(String title);

    @WebMethod
    void changeNews(NewsPaperModel newsPaperModel);
}

