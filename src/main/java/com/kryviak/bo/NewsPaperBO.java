package com.kryviak.bo;

import com.kryviak.model.NewsPaperModel;
import com.kryviak.utils.CsvController;

import java.util.ArrayList;
import java.util.List;

public class NewsPaperBO {

    public List<NewsPaperModel> findNewsByGenre(String genre) {
        List<NewsPaperModel> newsPapers = new CsvController().getAllNews();
        List<NewsPaperModel> newsPapersResult = new ArrayList<>();
        for (NewsPaperModel item : newsPapers) {
            if (item.getGenre().equalsIgnoreCase(genre)) {
                newsPapersResult.add(item);
            }
        }
        return newsPapersResult;
    }

    public boolean deleteNewsByTitle(String newsTitle) {
        if (isTitlePresent(newsTitle)) {
            List<NewsPaperModel> newsPapers = new CsvController().getAllNews();
            List<NewsPaperModel> newsPapersResult = new ArrayList<>();
            for (NewsPaperModel item : newsPapers) {
                if (!item.getTitle().equalsIgnoreCase(newsTitle)) {
                    newsPapersResult.add(item);
                }
            }
            new CsvController().writeDataToCSV(newsPapersResult);
            return true;
        } else return false;
    }

    public boolean isTitlePresent(String newsTitle) {
        List<NewsPaperModel> newsPapers = new CsvController().getAllNews();
        boolean flag = false;
        for (NewsPaperModel item : newsPapers)
            if (newsTitle.equals(item.getTitle()))
                flag = true;
        return flag;
    }

    public boolean addNews(NewsPaperModel newsPaperModel) {
        if (isObjectValid(newsPaperModel)) {
            List<NewsPaperModel> newsPapers = new CsvController().getAllNews();
            newsPapers.add(newsPaperModel);
            new CsvController().writeDataToCSV(newsPapers);
            return true;
        } else return false;
    }

    public boolean isObjectValid(NewsPaperModel newsPaperModel) {
        return (!newsPaperModel.getTitle().equals(""))
                && (!newsPaperModel.getDescription().equals(""))
                && (!newsPaperModel.getMessage().equals(""))
                && (!newsPaperModel.getMessageLink().equals(""))
                && (!newsPaperModel.getGenre().equals(""));
    }

    public void editNews(NewsPaperModel newsPaperModel) {
        List<NewsPaperModel> newsPapers = new CsvController().getAllNews();
        List<NewsPaperModel> newsPapersResult = new ArrayList<>();
        for (NewsPaperModel item : newsPapers) {
            if (item.getTitle().equalsIgnoreCase(newsPaperModel.getTitle())) {
                item.setTitle(newsPaperModel.getTitle());
                item.setDescription(newsPaperModel.getDescription());
                item.setMessage(newsPaperModel.getMessage());
                item.setMessageLink(newsPaperModel.getMessageLink());
                item.setGenre(newsPaperModel.getGenre());

                newsPapersResult.add(item);
            }
            newsPapersResult.add(item);
        }
        new CsvController().writeDataToCSV(newsPapersResult);
    }
}