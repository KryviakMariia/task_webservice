package com.kryviak.service.soap;

import com.kryviak.model.NewsPaperModel;

import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;


public class HelloWorldClient{

    public static void main(String[] args) throws Exception {

        URL url = new URL("http://localhost:9999/ws/hello?wsdl");

        //1st argument service URI, refer to wsdl document above
        //2nd argument is service name, refer to wsdl document above
        QName qname = new QName("http://soap.service.kryviak.com/", "NewsPaperSoapServiceImplService");

        Service service = Service.create(url, qname);

        NewsPaperSoapService hello = service.getPort(NewsPaperSoapService.class);

        NewsPaperModel newsPaperModel = new NewsPaperModel();
        newsPaperModel.setTitle("TTT");
        newsPaperModel.setDescription("DDD");
        newsPaperModel.setGenre("GGG");
        newsPaperModel.setMessageLink("MLMLML");
        newsPaperModel.setMessage("MMM");

        System.out.println(hello.getAllNews());

    }

}