package com.kryviak.service.soap;

import com.kryviak.bo.NewsPaperBO;
import com.kryviak.model.NewsPaperModel;
import com.kryviak.utils.CsvController;

import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "com.kryviak.service.soap.NewsPaperSoapService")
public class NewsPaperSoapServiceImpl implements NewsPaperSoapService {
    @Override
    public List<NewsPaperModel> getAllNews() {
        return new CsvController().getAllNews();
    }

    @Override
    public List<NewsPaperModel> getNews(String genre) {
        return new NewsPaperBO().findNewsByGenre(genre);
    }

    @Override
    public boolean addNews(NewsPaperModel newsPaperModel) {
        return new NewsPaperBO().addNews(newsPaperModel);
    }

    @Override
    public boolean removeNews(String title) {
        return new NewsPaperBO().deleteNewsByTitle(title);
    }

    @Override
    public void changeNews(NewsPaperModel newsPaperModel) {
        new NewsPaperBO().editNews(newsPaperModel);
    }
}
