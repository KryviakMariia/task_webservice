package com.kryviak.model;


import com.opencsv.bean.CsvBindByName;

public class NewsPaperModel {

    @CsvBindByName(column = "title", required = true)
    private String title;

    @CsvBindByName(column = "description", required = true)
    private String description;

    @CsvBindByName(column = "message", required = true)
    private String message;

    @CsvBindByName(column = "messageLink", required = true)
    private String messageLink;

    @CsvBindByName(column = "genre", required = true)
    private String genre;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageLink() {
        return messageLink;
    }

    public void setMessageLink(String messageLink) {
        this.messageLink = messageLink;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public String toString() {
        return "{title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", message='" + message + '\'' +
                ", messageLink='" + messageLink + '\'' +
                ", genre='" + genre + '\'' +
                '}';
    }
}
