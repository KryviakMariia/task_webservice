package com.kryviak;

public interface MessagesConstants {

    String NO_GENRE_FOUND = "Sorry, the Genre, you are trying to find is not exists. Please try another one.";
    String NO_TITLE_FOUND = "Sorry, the Title, you are trying to change is not exists. Please try another one.";
    String NEWSPAPER_WAS_DELETE = "Newspaper was deleted successfully";
    String NEWSPAPER_WAS_NOT_EXIST = "Sorry, the Newspaper contain invalid data";
    String NEWSPAPER_WAS_ADDED = "Newspaper was added successfully";
    String NEWSPAPER_WAS_CHANGE = "Newspaper was changed successfully";
}
