package com.kryviak.utils;

import com.kryviak.model.NewsPaperModel;
import com.opencsv.CSVWriter;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class CsvController {

    private static final String DB_CSV_FILE_NAME = "db.csv";

    public List<NewsPaperModel> getAllNews() {
        List<NewsPaperModel> paperModels = null;
        try (
                Reader reader = Files.newBufferedReader(Paths.get(new FileUtils().getFilePath(DB_CSV_FILE_NAME)));) {
            CsvToBean<NewsPaperModel> csvToBean = new CsvToBeanBuilder(reader)
                    .withType(NewsPaperModel.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();
            return csvToBean.parse();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return paperModels;
    }

    public void writeDataToCSV(List<NewsPaperModel> paperModels) {
        File file = new File(new FileUtils().getFilePath(DB_CSV_FILE_NAME));

        try {
            FileWriter outputfile = new FileWriter(file);

            CSVWriter writer = new CSVWriter(outputfile, ',',
                    CSVWriter.NO_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END);

            List<String[]> data = new ArrayList<String[]>();
            data.add(new String[]{"title", "description", "message", "messageLink", "genre"});
            for (NewsPaperModel item: paperModels) {
                data.add(new String[]{item.getTitle(), item.getMessage(), item.getDescription(), item.getMessageLink(),
                        item.getGenre()});
            }
            writer.writeAll(data);

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
